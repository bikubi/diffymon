srv "fooey" \
	"httpd mariadb" \
	"/home /boot /"

srv "barry" \
	"lighttpd" \
	"/ /mnt/baz"

srv "skippy" \
	"someservice" \
	"/some/mount"
