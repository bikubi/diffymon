#!/bin/bash

MATRIXSH=./matrix.sh/matrix.sh
MATRIXCONFIG=~/.matrix.sh

if [ ! -f "$MATRIXSH" ]; then
	echo "no matrix.sh at $MATRIXSH"
	exit 1
fi
if [ ! -f "$MATRIXCONFIG" ]; then
	echo "no matrix.sh config at $MATRIXCONFIG"
	exit 1
fi
if [ ! -f "env.sh" ]; then
	echo "no env.sh (pwd: $(pwd))"
	exit 1
fi
if [ ! -f "check.sh" ]; then
	echo "no check.sh (pwd: $(pwd))"
	exit 1
fi

function www {
	echo -n "$1 : "
	curl -s --show-error --fail --user-agent "bikubistatus 0.1" "$1" 2>&1 | grep -m1 -Po '(^curl:.*|<title.*?</title>)' | tr -d '\n'
	echo
}

function srv {
	label="$1"
	targetvar="srv_$label"
	target=${!targetvar}
	if [ "$target" = "#" ]; then
		echo "$label: skip"
		return
	fi
	services="$2"
	mnts="$3"
	$target /bin/bash << EOF
		for service in $services; do
			echo -n "$label: service \$service: "
			systemctl is-active \$service
		done
		for mnt in $mnts; do
			echo -n "$label: df \$mnt: "
			df --output=pcent \$mnt | grep -P '^(100| 9[5-9])' || echo '<95%'
		done
EOF
}

source env.sh
source check.sh > now.log

if [ ! -f last.log ]; then
	echo "no last.log, first run?"
	mv -v now.log last.log
	exit 0
fi

ts=$(date +%F_%H:%M:%S)
if diff last.log now.log; then
	echo "logs same, nothing to report"
	echo "$ts" >> diff.log
else
	diff=$(diff --side-by-side  --suppress-common-lines last.log now.log)
	echo -e "$ts\n$diff\n" >> diff.log
	$MATRIXSH "$HOST: $diff"
fi

mv now.log last.log

