# Diffémon

*Very* simple website and server monitoring.  
Takes a textual snapshot every hour, compares it to the last. If the diff is non-empty, this means that something must be wrong - then Diffémon alerts you through Matrix IM.

Two machines should monitor each other.

Not much tested yet, and systemically not super reliable. Use at your own risk.

## Structure

* `status.sh` is the main 'entrypoint' which is run every hour, e.g. by a systemd timer and service.
* `status.sh` provides helper functions to and calls your `check.sh`.
* `check.sh` outputs the current 'state' of the things you monitor, using the helper functions, and saves it. 
* On the next run, `status.sh` `diff`s the current to the last state and reports *differences* by instant message.

## Setup

Assuming two machines, Fooey and Barry.

1. Create your `check.sh` from `check_example.sh`. This file is meant to be shared by all machines, so copy it to Fooey and Barry. See below how to use the helper functions.
2. If you use the `srv` helper function, create individual `env.sh`s for Fooey and Barry.  Essentially, Fooey and Barry must know how to ssh into each other (or what to skip). Check `env_example.sh` and read on for details.
3. [Set up `matrix.sh`](https://github.com/fabianonline/matrix.sh), e.g.
    1. `git submodule update --init` in this repo
    2. you'll probably have to install its dependencies (likely `jq`)
    3. create an new Matrix account if needed
    4. create a new Matrix room and invite your new account into this room
    5. run `./matrix/matrix.sh --login`. This will interactively create `~/.matrix.sh` which holds the access token.
    6. run `./matrix/matrix.sh --select-default-room` to select the room you created before. For me, this took some tries; probably there is a delay.
    7. run `./matrix/matrix.sh "Hello"` to test messaging to the room.
    8. You can copy ~/.matrix.sh across Fooey and Barry etc.
4. copy `systemd/diffemon.service` to your `/etc/systemd/system`, set the user, group, path, etc. `systemctl daemon-relaod` and `systemctl start diffemon` to see if it works. A `last.log` should be created.
5. copy `systemd/hourly@bikubi-status.timer` there, `systemctl enable` and `start` it.

## Helper functions / commands to set up monitoring

### `www`

Is this website up and OK-ish?

```
www https://example/
```

Will `curl` and `grep` for `<title`.

* If curl fails (e.g. 404 or 500), this will trigger a diff
* If curl succeeds, but the site's `<title>` changes (e.g. because it says "Your Site - Not Found"), this will trigger a diff

### `srv` 

* Is this server up? 
* Are these systemd services `active`?
* Is there enough disk space on these mountpoints?

```
# srv label services mountpoints
srv "fooey" "service-a service-b" "/mnt/a /mnt/b /home /"
```

* The first argument to `srv` provides a label for `env.sh`, but will also show up in the diff. (`HOST` from `env.sh` will tell you who reported the diff.)
* The command above will ssh into the server labeled `fooey`, with the method provided in `env.sh`, i.e.  
* Fooey and Barry will check their own systemd services, too. Leave the config blank here.
* If one machine can't, or shouldn't reach one other, set a `#`.
* `~/.ssh/config`, `authorized_keys` and `ssh-copy-id` are your friends.

Example `env.sh`s:

```sh
# on barry:
HOST="barry"
srv_fooey="ssh user@fooey.example.com"
srv_barry=""

# on fooey, vice versa:
HOST="fooey"
srv_fooey=""
srv_barry="ssh barry"
```

Back to the command:

```
srv "fooey" "service-a service-b" "/mnt/a /mnt/b /home /"
```

* If ssh fails, this will trigger a diff
* The second argument is a space-separated list of systemd services to check `is-active`. If one isn't, this will trigger a diff
* The last arguments checks "disk free". Anything under 95% will be reported as `<95%`. So if this changes to ` 96%` this will trigger a diff (and also to ` 97%`, or back to `<95%`).

## Extensibility

Helper functions are just bash functions that output a line (that should not change unless something's wrong).

## TODOs

* [ ] refactor the structure into `main.sh`, `lib/helper_www.sh`, `lib/diff.sh` and so on.
* [ ] add a helper function `backupage` to check if backups are fresh enough
* [ ] add info how to create restricted ssh users
* [ ] add check for jq installed

## License

MIT
